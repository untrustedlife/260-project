
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.*;
import java.util.*;

public class SearchByPublisherPanel extends JPanel 
	{

	private static final long serialVersionUID = 1L;

	public SearchByPublisherPanel(final HenryDAO data) 
		{
		//will loop through vector and create jlabels

		setBackground(Color.GREEN);
		BorderLayout layout =  new BorderLayout();
		layout.setHgap(0);
		layout.setVgap(10);
		setLayout(layout);
		setVisible(true);
		
		
		//Display
		final JTextField search = new JTextField();
		search.setSize(250, 100);
		search.setMinimumSize(new Dimension(250,200));
		search.setVisible(true);
		search.setEnabled(false);	
		
		//Publisher list
		final JComboBox<String> pub= new JComboBox<String>(data.getPublisherData());
		pub.setSelectedIndex(0);
		pub.setSize(250, 500);
		pub.setVisible(true);
		
		
		//Book List
		final JComboBox<String> book= new JComboBox<String>(data.getBookData(0, pub.getItemAt(0)));
		book.setSelectedIndex(0);
		book.setSize(250,500);
		book.setVisible(true);

		
		//Branch data
		final DefaultListModel<String> model = new DefaultListModel<String>();
		final JList<String> list = new JList<String>(model);
		Vector<String> items = data.getBranchData(book.getItemAt(0));
		for(int i = 0; i < items.size(); ++i)
		{	
			model.addElement(items.get(i));
		}

		list.setVisible(true);
		
		
		if (data.getBookData(3, book.getItemAt(book.getSelectedIndex())).get(0) != null)
			search.setText(data.getBookData(3, book.getItemAt(0)).get(0));

		  book.addActionListener(new AbstractAction("book") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
			model.clear();
			Vector<String> items = data.getBranchData(book.getItemAt(book.getSelectedIndex()));
			
			for(int i = 0; i < items.size(); ++i)
			{	
				model.addElement(items.get(i));
			}
			if (data.getBookData(3, book.getItemAt(book.getSelectedIndex())).get(0) != null)
				search.setText(data.getBookData(3, book.getItemAt(book.getSelectedIndex())).get(0));

			
			}
		});
		
		pub.addActionListener(new AbstractAction("pub") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				book.removeAllItems();

				Vector<String> items = data.getBookData(0, pub.getItemAt(pub.getSelectedIndex()));
				
				for(int i = 0; i < items.size(); ++i)
				   book.addItem(items.get(i));

				
			}
		});
		
		add(search, BorderLayout.PAGE_START);
		add(pub, BorderLayout.PAGE_END);
		add(book, BorderLayout.LINE_START);
		add(list, BorderLayout.CENTER);
		
		}

	}
