
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.*;
import java.util.*;

public class SearchByAuthorPanel extends JPanel 
	{
	private static final long serialVersionUID = 1L;

	/*
	 * I�ll describe the SearchByAuthorPanel here, but the other two panels are similar.  
	 * You will need a JComboBox to select the author.  Note that when you create such a box, 
	 * you can provide a Vector of objects you want displayed in the box.  Since you want this
	 * filled from the start with all the authors, you will get this list of author data from the DAO
	 * (see below).  If you want to work on the GUI first, you can always make a Vector of fake info 
	 * and see what it looks like visually before attaching to the DAO.  You will also need to setup an 
	 * event handler for the box � specifically an action listener.  I would suggest an anonymous inner 
	 * class for this task.  This is fairly common for GUI work.  It is this code that will get executed 
	 * when the user selects a new author from the box.  Thus, it is its job to find which author was selected
	 * and then go to the DAO and get the book information associated with that author.  
	 * Your book JComboBox should already be visible on the screen, so you will need to remove all the old books
	 * from this box and add all the new ones to it. 
	 */
	
	
	public SearchByAuthorPanel(final HenryDAO data) 
		{
		//will loop through vector and create jlabels

		setBackground(Color.RED);
		BorderLayout layout =  new BorderLayout();
		layout.setHgap(0);
		layout.setVgap(10);
		setLayout(layout);
		setVisible(true);
		
		
		//Display
		final JTextField search = new JTextField();
		search.setSize(250, 100);
		search.setMinimumSize(new Dimension(250,200));
		search.setVisible(true);
		search.setEnabled(false);	  
		
		
        //Publisher list
		final JComboBox<String> auth= new JComboBox<String>(data.getAuthorData());
		auth.setSelectedIndex(0);
		auth.setSize(250, 500);
		auth.setVisible(true);

		
		//Book List
		final JComboBox<String> book= new JComboBox<String>(data.getBookData(2, auth.getItemAt(0)));
		book.setSelectedIndex(0);
		book.setSize(250,500);
		book.setVisible(true);

		//Branch data
		final DefaultListModel<String> model = new DefaultListModel<String>();
		final JList<String> list = new JList<String>(model);
		Vector<String> items = data.getBranchData(book.getItemAt(0));
		for(int i = 0; i < items.size(); ++i)
		{	
			model.addElement(items.get(i));
		}

		list.setVisible(true);
		
		
		if (data.getBookData(3, book.getItemAt(book.getSelectedIndex())).get(0) != null)
			search.setText(data.getBookData(3, book.getItemAt(0)).get(0));
		
		  book.addActionListener(new AbstractAction("book") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
			model.clear();
			Vector<String> items = data.getBranchData(book.getItemAt(book.getSelectedIndex()));
			
			for(int i = 0; i < items.size(); ++i)
			{	
				model.addElement(items.get(i));
			}
			if (data.getBookData(3, book.getItemAt(book.getSelectedIndex())).get(0) != null)
				search.setText(data.getBookData(3, book.getItemAt(book.getSelectedIndex())).get(0));

			
			}
		});
		
		auth.addActionListener(new AbstractAction("author") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				book.removeAllItems();

				Vector<String> items = data.getBookData(2, auth.getItemAt(auth.getSelectedIndex()));
				
				for(int i = 0; i < items.size(); ++i)
				   book.addItem(items.get(i));

				
			}
		});
		
		add(search, BorderLayout.PAGE_START);
		add(auth, BorderLayout.PAGE_END);
		add(book, BorderLayout.LINE_START);
		add(list, BorderLayout.CENTER);
		}
	
	
	}
