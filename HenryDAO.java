import java.util.Vector;
import java.sql.*;

/*
 * Make sql calls here
 * Querying a SQL database with JDBC is typically a three-step process:
 * Create a JDBC ResultSet object.
 * Execute the SQL SELECT query you want to run.
 * Read the results.
 */
public class HenryDAO 
	{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "oracle.jdbc.OracleDriver";  
	   static final String DB_URL = "jdbc:oracle:thin:@dario.cs.uwec.edu:1521:csdev";

	   //  Database credentials
	   static final String USER = "HAMMMR0216";
	   static final String PASS = "TK2ERP3S";
	   
	   //STEP 2: Register JDBC driver
	   Connection cn = null;
	   Statement stmt = null;
	   ResultSet rs= null;

	/*
     *Constructor
	 */
	public HenryDAO() 
		{
			try 
				{
				Class.forName(JDBC_DRIVER);
				} 
			catch (ClassNotFoundException e) 
				{
				e.printStackTrace();
				}
		}
	
	/*
	 *call an sql statement with the column you are searching for
	 */
	public Vector<String> callSql(String statement, String searchString)
		{
			try 
				{
				cn = DriverManager.getConnection(DB_URL,USER,PASS);
				//STEP 4: Execute a query
				System.out.println("Creating statement...");
				stmt = cn.createStatement();
				rs = stmt.executeQuery(statement); 
				//Read through rs
				Vector<String> dataTable = new Vector<String>();
				
				   while (rs.next()) {
				        dataTable.add(rs.getString(searchString));
				    }
				   closeSQL();
					return dataTable;
				} 
				catch (SQLException e) 
				{
				e.printStackTrace();
				}
			return null;

		}
	
	/*
	* Close sql connections
	*/
	public void closeSQL()
		{
		try 
			{
			rs.close();
			stmt.close();
			cn.close();
			} 
		catch (SQLException e) 
			{
			e.printStackTrace();
			}
		}
	
	/*
	 * Grab author data
	 */
	public Vector<String> getAuthorData()
		{
		 Vector<String> vec = new Vector<String>();
		 Vector<String> vec2 = new Vector<String>();
		 vec.addAll(callSql(""
		 		+ "SELECT AUTHOR_LAST "
		 		+ "FROM HENRY_AUTHOR", "author_last")
				 );
		 vec2.addAll(callSql(""
			 	+ "SELECT AUTHOR_FIRST "
			 	+ "FROM HENRY_AUTHOR", "author_first")
				);
		 
		 //combine two vectors to get proper author name
		 Vector<String> vec3 = new Vector<String>();
		 for (int i = 0; i<vec2.size(); i++)
		 	{
			 vec3.add(vec2.get(i).trim());
		 	}
		 for (int i = 0; i<vec3.size(); i++)
		 	{
			 vec3.set(i,  vec3.get(i) + " " + vec.get(i).trim());
		 	}
		 return vec3;
		}
	
	/*
	 * Grab books by author name
	 */
	public Vector<String> getBookByAuthor(String authorsName)
	{
	 String[] namesplit = authorsName.split(" ");
	 String author_first = namesplit[0];
	 String author_last = namesplit[1];
		System.out.println(authorsName);
		System.out.println(author_first);
		System.out.println(author_last);
	 return   callSql(   
		      "SELECT TITLE "+
		      "FROM HENRY_BOOK JOIN " +
		      "HENRY_WROTE ON " +
			  "HENRY_BOOK.BOOK_CODE = HENRY_WROTE.BOOK_CODE JOIN " +
		      "HENRY_AUTHOR ON " + 
		      "HENRY_AUTHOR.AUTHOR_NUM = HENRY_WROTE.AUTHOR_NUM " +
		      "WHERE HENRY_AUTHOR.AUTHOR_LAST LIKE " + "'%" + author_last+ "%'" + 
		      " AND " +
		      "HENRY_AUTHOR.AUTHOR_FIRST LIKE " + "'%" + author_first + "%'"
		      , "title"
		      );

	}
	
	/*
	 * Grab all categories
	 */
	public Vector<String> getCategoryData()
		{
		 Vector<String> vec = new Vector<String>();
		 vec.addAll(callSql(""
		 		+ "SELECT DISTINCT TYPE "
		 		+ "FROM HENRY_BOOK "
		 		+ "ORDER BY TYPE", "type")
				 );
		return vec;
	
		}
	
	/*
	 * Grab all publishers
	 */
	public Vector<String> getPublisherData()
		{
		 Vector<String> vec = new Vector<String>();
		 vec.addAll(callSql(
				 "SELECT PUBLISHER_NAME " +
                "FROM HENRY_PUBLISHER " +
                "ORDER BY PUBLISHER_NAME", "publisher_name")
				 );
		return vec;
		}
	
	/*
	 * Grab and concatenate the branch data
	 */
	public Vector<String> getBranchData(String bookName)
	{
	 Vector<String> vec = new Vector<String>();
	 Vector<String> vec1 = new Vector<String>();
	 vec.addAll(callSql(
			 "SELECT BRANCH_NAME " +
            "FROM HENRY_BRANCH JOIN " +
			"HENRY_INVENTORY ON "+
			"HENRY_BRANCH.BRANCH_NUM = HENRY_INVENTORY.BRANCH_NUM JOIN "+
			"HENRY_BOOK ON "+
			"HENRY_BOOK.BOOK_CODE = HENRY_INVENTORY.BOOK_CODE "+
			"WHERE TITLE LIKE " + "'" +  bookName +"'"+ 
            " ORDER BY BRANCH_NAME", "branch_name")
			 );
	 vec1.addAll(callSql(
		    "SELECT ON_HAND " +
		    "FROM HENRY_BRANCH JOIN " +
			"HENRY_INVENTORY ON " + 
			"HENRY_BRANCH.BRANCH_NUM = HENRY_INVENTORY.BRANCH_NUM JOIN " +
			"HENRY_BOOK ON " +
			"HENRY_BOOK.BOOK_CODE = HENRY_INVENTORY.BOOK_CODE " +
			"WHERE TITLE LIKE " + "'" + bookName +"'" + 
		     " ORDER BY BRANCH_NAME", "on_hand")
			 );
	 
	//combine two vectors to get proper displayable data
	 Vector<String> vec3 = new Vector<String>();
	 for (int i = 0; i<vec.size(); i++)
	 	{
		 vec3.add(vec.get(i));
	 	}
	 for (int i = 0; i<vec3.size(); i++)
	 	{
		 vec3.set(i,  vec3.get(i) + " " +  vec1.get(i));
	 	}
	 return vec3;
	}


	/*
	 * Grab the book titles and the price (all under different statements with different data)
	 */
	public Vector<String> getBookData(int index, String name)
		{
		Vector<String> vec = new Vector<String>();	
		if (name != null)
		{
	    String name2 = name;
	    name = "'"+name+"'";
		switch (index)
		{
		case 0:
			/*
			 * Grab the books by publisher
			 */
			vec.addAll(callSql(
					"SELECT TITLE, PUBLISHER_NAME " +
					"FROM HENRY_PUBLISHER JOIN HENRY_BOOK ON " +
					"HENRY_PUBLISHER.PUBLISHER_CODE = HENRY_BOOK.PUBLISHER_CODE " +
					"WHERE HENRY_PUBLISHER.PUBLISHER_NAME = " + name, "title"
					));
			if (vec.size()== 0)
				{
				vec.add("No Books Available");
				}
			break;
	
		case 1:
			/*
			 * Grab the books by category
			 */
		     vec.addAll(callSql(
		      "SELECT TITLE "+
		      "FROM HENRY_BOOK "+
		      "WHERE TYPE LIKE " + name, "title"
		      ));
			if (vec.size()== 0)
				{
				vec.add("No Books Available");
				}
			break;
		
		case 2:
			vec.addAll(getBookByAuthor(name2));
			if (vec.size()== 0)
			{
			vec.add("No Books Available");
			}
			break;
			
		case 3:
			//grab price for book
			 vec.addAll(callSql(
					 "SELECT PRICE, TITLE \n" +
	                "FROM HENRY_BOOK " +
	                "WHERE TITLE LIKE " + name, "price")
					 );
			 
				if (vec.size()== 0)
				{
				vec.add("No Price Available");
				}
			break;
		
		}
		return vec;
		}
		else
		{
			//Failsafe data in case something weird happens, but it shouldnt
			vec.add("Thisshouldnthappen");
		}
		return vec;
		}
	}
